import AnalyticRepository from "~/core/domain/repositories/analytic/AnalyticRepository";
import ComplianceRepository from "~/core/domain/repositories/compliance/ComplianceRepository";
import CustomerRepository from "~/core/domain/repositories/customer/CustomerRepository";
import HTTPClientRepository from "~/core/domain/repositories/http/HTTPClientRepository";
import SharedValuesRepository from "~/core/domain/repositories/shared/SharedValuesRepository";
import TransactionRepository from "~/core/domain/repositories/transaction/TransactionRepository"
import UtilsRepository from "~/core/domain/repositories/utils/UtilsRepository";
import MarketRepository from "~/core/domain/repositories/market/MarketRepository";
import AnalyticService from "../http/AnalyticService";
import ComplianceService from "../http/ComplianceService";
import CustomerService from "../http/CustomerService";
import KuzzleService from "../http/KuzzleService";
import TransactionService from "../http/TransactionService";
import UtilService from "../http/UtilService";
import AccountsRepository from "~/core/domain/repositories/accounts/AccountsRepository";
import AccountsService from "../http/AccountsService";
import MarketService from "../http/MarketService";

export function loadHTTPServices(api: HTTPClientRepository) {
    const services = {
        customer: new CustomerService(api),
        compliance: new ComplianceService(api),
        kuzzle: new KuzzleService(api),
        analytic: new AnalyticService(api),
        utils: new UtilService(api),
        accounts: new AccountsService(api),
        transaction: new TransactionService(api),
        market: new MarketService(api),
      };
    return services;
}

export type LoadHTTPServicesType = {
  customer: CustomerRepository,
  compliance: ComplianceRepository,
  kuzzle: SharedValuesRepository,
  analytic: AnalyticRepository,
  utils: UtilsRepository,
  accounts: AccountsRepository,
  transaction: TransactionRepository,
  market: MarketRepository
}
