import HTTPClientRepository from "~/core/domain/repositories/http/HTTPClientRepository";
import UtilsRepository from "~/core/domain/repositories/utils/UtilsRepository";
import { JSONObject } from "~/types/common";

export default class UtilService implements UtilsRepository {
  axios: HTTPClientRepository;
  constructor(axios: HTTPClientRepository) {
    this.axios = axios;
  }

  async findUtils(): Promise<JSONObject> {
    const data = await this.axios.get(`/utils`);
    return data.data;
  }
}
