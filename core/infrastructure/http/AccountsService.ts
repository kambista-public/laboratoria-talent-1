//@ts-nocheck
import localStorage from "localStorage";
import AccountsRepository from "~/core/domain/repositories/accounts/AccountsRepository";
import HTTPClientRepository from "~/core/domain/repositories/http/HTTPClientRepository";
import { JSONObject } from "~/types/common";

export default class AccountsService implements AccountsRepository {
  axios: HTTPClientRepository;
  constructor(axios: HTTPClientRepository) {
    this.axios = axios;
  }

  async createAccount(data) {
    try {
      const res = await this.axios.post("/accounts", data);
      const accounts = JSON.parse(localStorage.getItem("accounts")) || [];
      accounts.push(res.data.data);
      localStorage.setItem("accounts", JSON.stringify(accounts));
      return res.data;
    } catch (error) {
      return error.response;
    }
  }

  async searchAccount() : Promise<JSONObject> {
    try{
      const res = await this.axios.get("/accounts?currency=EUR");
      localStorage.setItem("accounts", JSON.stringify(res.data.data));
      return res.data;
    } catch (error) {
      return error.response;
    }
  }

  async updateAccount(accountId, data) {
    try {
      const res = await this.axios.put(`/accounts/${accountId}`, data);
      const accounts = JSON.parse(localStorage.getItem("accounts"));
      const updatedAccounts = accounts.map((account) => {
        return account.id === accountId ? { ...res.data.data } : account
      });
      localStorage.setItem("accounts", JSON.stringify(updatedAccounts));
      return res.data;
    } catch (error) {
      return error.response;
    }
  }

  async deleteAccount(accountId) {
    try {
      const res = await this.axios.delete(`/accounts/${accountId}`);
      const accounts = JSON.parse(localStorage.getItem("accounts"));
      const deletedAccount = accounts.filter((account) => account.id !== accountId);
      localStorage.setItem("accounts", JSON.stringify(deletedAccount));
      return res.data;
    } catch (error) {
      return error.response;
    }
  }

  getAccounts() {
    return JSON.parse(localStorage.getItem("accounts"));
  }
}