import HTTPClientRepository from "~/core/domain/repositories/http/HTTPClientRepository";
import
MarketRepository from "~/core/domain/repositories/market/MarketRepository";
import { JSONObject } from "~/types/common";

export default class MarketService implements MarketRepository {
  axios: HTTPClientRepository;
  constructor(axios: HTTPClientRepository) {
    this.axios = axios;
  }

  async findMarket(): Promise<JSONObject> {
    const data = await this.axios.get(`/markets`);
    return data.data;
  }
}
