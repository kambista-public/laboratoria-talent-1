//@ts-nocheck
// import Cookies from "js-cookie";
// import localStorage from "localStorage";
import TransactionRepository from "~/core/domain/repositories/transaction/TransactionRepository";
import HTTPClientRepository, { Response } from "~/core/domain/repositories/http/HTTPClientRepository";
import { JSONObject } from "~/types/common";

export default class  TransactionService implements TransactionRepository {
  axios: HTTPClientRepository;
  constructor(axios: HTTPClientRepository) {
    this.axios = axios;
  }

  async createTransaction(data) : Response<JSONObject>  {
    let endpoint = `/transactions`;
    const res = await this.axios.post(endpoint, data);
    return res;
  }

  async checkTransaction(transactionId, data) : Response<JSONObject> {
    let endpoint = `/transactions/${transactionId}/check`;
    const res = await this.axios.post(endpoint, data);
    return res;
  }
}
