import { JSONObject } from "~/types/common";

export default interface MarketRepository {
    findMarket(): Promise<JSONObject>
}