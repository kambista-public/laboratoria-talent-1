import { JSONObject } from "~/types/common";

export default interface CustomerRepository {
  createTransaction(data: JSONObject): Promise<any>;
  checkTransaction(transactionId: string): Promise<any>;
}