import { JSONObject } from "~/types/common";
// import { Response } from "../http/HTTPClientRepository";

export default interface UtilsRepository {
  findUtils(): Promise<JSONObject>;
}
