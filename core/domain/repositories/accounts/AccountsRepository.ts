import { JSONObject } from "~/types/common";
export default interface AccountsRepository {
  getAccounts(): Array<Object>;
  createAccount(data: JSONObject):Promise<JSONObject>;
  searchAccount():Promise<JSONObject>;
  updateAccount(accountId: string):Promise<JSONObject>;
  deleteAccount(accountId: string):Promise<JSONObject>;
}